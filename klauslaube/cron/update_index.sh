#! /bin/bash

cd ~/webapps/klauslaube;
source env/bin/activate;
python manage.py update_index --settings=settings.production;
deactivate;
