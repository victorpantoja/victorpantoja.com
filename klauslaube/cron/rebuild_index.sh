#! /bin/bash

cd ~/webapps/klauslaube;
source env/bin/activate;
python manage.py rebuild_index --settings=settings.production;
deactivate;
