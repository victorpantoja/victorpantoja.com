import os
from settings.defaults import *

DEBUG = True
TEMPLATE_DEBUG = DEBUG
LOCAL = True

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(PROJECT_ROOT_PATH, 'test.sqlite3'),
        'USER': '',
        'PASSWORD': '',
        'HOST': '',
        'PORT': '',
    }
}

MEDIA_ROOT = os.path.join(PROJECT_ROOT_PATH, 'media_tests')

INSTALLED_APPS += ('django_coverage', )

# Compressor settings
COMPRESS_ENABLED = False

# Haystack settings
HAYSTACK_CONNECTIONS = {
	'default': {
		'ENGINE': 'haystack.backends.whoosh_backend.WhooshEngine',
        'PATH': os.path.join(PROJECT_ROOT_PATH, 'whoosh_index_tests'),
    }
}

# Coverage settings
from django_coverage.settings import *

COVERAGE_MODULE_EXCLUDES += [
    # packages
    'diario',
    'haystack',
    'imagekit',
    'south',
    'tagging',
    'compressor',

    # modules
    'admin',
    'feeds',
    'fixtures',
    'managers',
    'search_indexes',
    'sitemaps',
]
