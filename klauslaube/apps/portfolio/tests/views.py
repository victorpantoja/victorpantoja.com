from django.core.urlresolvers import reverse
from django.test import TestCase

from portfolio.models import Job


class PortfolioViewsTests(TestCase):
    fixtures = ['portfolio_testdata.json', ]
    urls = 'urls'

    def test_job_list(self):
        url = reverse('portfolio:job_list')
        response = self.client.get(url)
        self.assertEquals(response.status_code, 200)

        self.assertEquals(len(response.context['object_list']), 3)

    def test_job_detail(self):
        # Active job
        job = Job.objects.get(pk=1)
        response = self.client.get(job.get_absolute_url())
        self.assertEquals(response.status_code, 200)
        self.assertEquals(response.context['job'], job)

        # Deactive job
        job = Job.objects.get(pk=3)
        job.is_active = False
        job.save()

        response = self.client.get(job.get_absolute_url())
        self.assertEquals(response.status_code, 404)
