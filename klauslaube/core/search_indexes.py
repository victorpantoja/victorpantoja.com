from haystack import indexes
from diario.models import Entry


class EntryIndex(indexes.SearchIndex, indexes.Indexable):
    """
    Creates a search index to entries from Diario app.
    """
    text = indexes.CharField(document=True, use_template=True)
    author = indexes.CharField(model_attr='author')
    pub_date = indexes.DateTimeField(model_attr='pub_date')
    tags = indexes.MultiValueField()

    def get_model(self):
        """
        Return the Diario entries model.
        """
        return Entry

    def index_queryset(self):
        """
        Return only published entries from blog.
        """
        return Entry.published_on_site

    def prepare_tags(self, obj):
        """
        Return a list of all tags from entry.
        """
        return obj.tags
