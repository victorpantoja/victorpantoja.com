from django.contrib.auth.models import User
from django.test import TestCase
from diario.models import Entry


class CoreSignalsTests(TestCase):
    def setUp(self):
        self.user = User.objects.create(username='test')
        self.post = Entry(
            title='Test post',
            slug='test-post',
            author=self.user,
            body_source=('<h2>Hi!</h2>'
                '[slideshare id=8354309'
                '&doc=canvasvssvgdavidsonfellipe-110619115605-phpapp02]'),
        )
        self.post.save()

    def test_slideshare_shortcode(self):
        """
        Should convert the slideshare shortcode to a iframe when save
        the blog entry.
        """
        self.assertTrue(('<iframe '
            'src="http://www.slideshare.net/slideshow/embed_code/8354309" '
            'width="590" height="481" frameborder="0" marginwidth="0" '
            'marginheight="0" scrolling="no"></iframe>'), self.post.body)
