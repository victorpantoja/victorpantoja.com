import re
from django.template import Library

register = Library()

SPLIT_PATTERN = '<!-- readmore -->'
SPLIT_WRAPPED_PATTERN = '<[\w\s]+>\s?%s\s?<\/[\w\s]+>' % SPLIT_PATTERN
OPENED_ELEMENTS_PATTERN = '\<\w+\>\s*$'


@register.simple_tag
def current(path, pattern, klass="current"):
    """
    Returns a CSS class if path corresponds at pattern.
    
    Usage::
        
        {% load core_extras %}
        <ul>
            <li {% current request.path "^/about/$" %}>About</li>
            <li {% current request.path "^contact/$" %}">Contact</li>
        </ul>
    """
    if re.search(pattern, path):
        return klass

    return ''


@register.filter(name='readmore')
def readmore(content):
    """
    Breaks the content into two parts, and returns the part that
    precedes the pattern "<!-- readmore -->".
    """
    if SPLIT_PATTERN in content:
        content = content.split('<!-- readmore -->')[0]
        # Check for opened elements
        while re.search(OPENED_ELEMENTS_PATTERN, content):
            content = re.sub(OPENED_ELEMENTS_PATTERN, '', content)

    return content


@register.filter(name='full_content')
def full_content(content):
    """
    Replaces the default "<!-- readmore -->" to a anchor called
    #readmore.
    """
    return re.sub(SPLIT_WRAPPED_PATTERN, '<span id="readmore"></span>', content)
